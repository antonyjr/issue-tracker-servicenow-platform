import requests
import json
from flask import Flask,jsonify,render_template,redirect,request


app = Flask("Issue Tracker")

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/newissue', methods=['POST'])
def newissue():
    url = 'https://yourdev.service-now.com/api/now/table/'
    # 'https://devXXXXX.service-now.com/api/now/table/x_793741_issue_tra_issues'
    
    user = 'admin' # 'admin'
    pwd = 'password' # 'Your Password'
    headers = {"Content-Type":"application/xml","Accept":"application/json"}

    if not request.is_json:
        return jsonify({"error" : True, "message" : "Invalid JSON"})
    
    content = request.get_json()
    title = None
    desc = None
    try:
        title = content["title"]
        desc = content["desc"]
    except:
        return jsonify({"error": True, "message": "Invalid JSON"})

    formdata = "<request><entry><title>{}</title><description>{}</description></entry></request>".format(title, desc)

    response = requests.post(url, auth=(user, pwd), headers=headers, data=formdata)
    if response.status_code != 201: 
        return jsonify({"error": True, "message": "API Request Failed, Try Again."})
    
    js = response.json() #.replace("'", '"')  
    #js = json.loads(data)

    return jsonify({"error": False, "message": "New Issue Opened Successfully ({})".format(js["result"]["number"])})

if __name__ == "__main__":
    app.run()
